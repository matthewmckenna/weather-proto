##############
Weather Proto
##############

Code for weather prototype.

Python Installation
====================
#. Download and install `Miniconda`_.
#. Open up a command window.
#. Install the ``requests`` library by running::

    pip install requests


Configuration
==============
#. Extract the code folder to a location on your disk.
#. Inside this folder, there will be a file ``sample.cfg.ini``. Make a copy of this file and rename
   it to ``cfg.ini``.
#. In this file, replace ``<YOUR API KEY>`` with your API key obtained from `OpenWeatherMap`_.
   Remember to also remove the angle brackets ``<>``.


Examples
=========

Current weather
----------------
To get the current weather in Drumcondra::

   python weather.py drumcondra,dublin,ie

Output::

  Current weather for "drumcondra,dublin,ie":
    Description: few clouds

    Latitude: 53.37000 Longitude: -6.27000
    Sunrise: 2017-03-06 06:58:42, Sunset: 2017-03-06 18:14:24
    Temp: 3 C, Pressure: 1013 hPa, Humidity: 86%
    Wind speed: 6.2 m/s (22.32 km/h)

Weather forecast 3 hours from now
----------------------------------

To get the weather forecast for Drumcondra (three hours from now)::

   python weather.py -f drumcondra,dublin,ie

or

::

   python weather.py --forecast drumcondra,dublin,ie

Output::

  Forecast for "drumcondra,dublin,ie" at 00:00:00:
    Description: few clouds

    Latitude: 53.36710 Longitude: -6.26830
    Temp: 4.66 C, Pressure: 1024.71 hPa, Humidity: 100%
    Wind speed: 7.88 m/s (28.37 km/h)


Troubleshooting
================

If you run into any issues, please do the following:

#. Run the command with the ``-v`` or ``--verbose`` command line option. This will create a log file
   in the same directory called ``.weather.log``.
#. Attach any output from the console. You can take a screenshot or copy and paste the contents into
   a text file.

Send both of the above to the author.

.. Links
.. _Miniconda: https://repo.continuum.io/miniconda/Miniconda3-latest-Windows-x86_64.exe
.. _OpenWeatherMap: https://openweathermap.org/

