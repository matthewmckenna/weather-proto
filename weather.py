#!/usr/bin/env python3
"""
Prints out the weather for a location passed via command
line.
"""
import argparse
import configparser
from datetime import datetime
import logging
import os

import requests


def process_args():
    """Parse command line arguments"""
    # Set up the parser
    parser = argparse.ArgumentParser(
        description='Get weather information for a given location.',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    # `+` will generate an error if there wasn't at least one argument
    parser.add_argument('location', nargs='+')

    # return forecast data rather than current data
    parser.add_argument(
        '-f',
        '--forecast',
        help='return forecast data instead of current data',
        action='store_true',
    )

    # Add `verbose` option for diagnostics
    parser.add_argument(
        '-v',
        '--verbose',
        help='enable logging to file',
        action='store_true',
    )

    return parser.parse_args()


def setup_logging(filename_root):
    """Set up some basic file logging.

    The log file is created in the folder in which the script is
    executed.
    """
    logging.basicConfig(
        filename='.{}.log'.format(filename_root),
        format='%(asctime)s %(message)s',
        level=logging.DEBUG
    )


def get_weather_data(url):
    """Make request to openweathermap.org and return result."""
    # make the request
    r = requests.get(url)

    # this will raise an exception if there were any issues making
    # the API call. this stops us attempting to work with bad data later on.
    r.raise_for_status()
    return r


def read_api_key():
    """Return API key from configuration file."""
    config = configparser.ConfigParser()
    config.read('cfg.ini')
    return config['openweathermap']['api_key']


def build_url(loc, key, forecast, *, units='metric', cnt=3):
    """Return the URL for the API request.

    Args:
        loc: location of interest in string format
        key: users API key
        forecast: flag to choose 3 hour forecast or current weather API
        units: format to return temperature data (metric returns Celcius)
        cnt: number of forecasts to return (more useful for forecast API)

    Returns:
        correctly formed URL to be used in API request
    """
    # setup the base URL
    base_url = 'http://api.openweathermap.org/data/2.5/{}?q={}&appid={}&units={}&cnt={}'

    # forecast API is available at `forecast`, current weather API is available
    # at `weather`
    api_type = 'forecast' if forecast else 'weather'

    return base_url.format(api_type, loc, key, units, cnt)


def main(args):
    """Main entry point for this application"""
    # create a string from the input location list
    loc = ' '.join(args.location)

    # read the users API key from the file ``cfg.ini``
    key = read_api_key()

    # build the URL using the location and the users API key
    url = build_url(loc, key, args.forecast)

    # log the url used in API request
    logging.info(url)

    try:
        response = get_weather_data(url)
    except requests.exceptions.ConnectionError as e:
        logging.exception(e)
    else:
        # decode to JSON
        data = response.json()

    # log the full response
    logging.info(data)

    # output the data to the screen
    if args.forecast:
        display_forecast_info(loc, data)
    else:
        display_weather_information(loc, data)


def display_forecast_info(loc, data):
    """Output forecast for the next 3 hours.

    Args:
        loc: the location of interest in string format
        data: JSON response from the API request
    """
    forecast_time = data['list'][0]['dt_txt']
    print('Forecast for "{}" at {}:'.format(loc, forecast_time[-8:]))

    description = data['list'][0]['weather'][0]['description']
    print('  Description: {}\n'.format(description))

    print('  Latitude: {lat:.5f} Longitude: {lon:.5f}'.format(**data['city']['coord']))

    print('  Temp: {} C, Pressure: {} hPa, Humidity: {}%'.format(
        data['list'][0]['main']['temp'],
        data['list'][0]['main']['pressure'],
        data['list'][0]['main']['humidity'],
        )
    )

    wind_speed_m_s = data['list'][0]['wind']['speed']
    # convert from m/s to km/h
    wind_speed_km_h = (wind_speed_m_s*60*60) / 1000

    print('  Wind speed: {} m/s ({:.2f} km/h)'.format(wind_speed_m_s, wind_speed_km_h))

    try:
        print('  Rainfall in past 3 hrs: {} mm'.format(data['list'][0]['rain']['3h']))
    except KeyError as e:
        # no rainfall data available, make a note
        logging.debug(e)


def display_weather_information(loc, data):
    """Output weather information to the screen.

    Args:
        loc: the location of interest in string format
        data: JSON response from the API request
    """
    print('Current weather for "{}":'.format(loc))

    description = data['weather'][0]['description']
    print('  Description: {}\n'.format(description))

    print('  Latitude: {lat:.5f} Longitude: {lon:.5f}'.format(**data['coord']))

    sunrise = datetime.utcfromtimestamp(data['sys']['sunrise'])
    sunset = datetime.utcfromtimestamp(data['sys']['sunset'])
    print('  Sunrise: {}, Sunset: {}'.format(sunrise, sunset))

    print('  Temp: {} C, Pressure: {} hPa, Humidity: {}%'.format(
        data['main']['temp'],
        data['main']['pressure'],
        data['main']['humidity'],
        )
    )

    wind_speed_m_s = data['wind']['speed']
    wind_speed_km_h = (wind_speed_m_s*60*60) / 1000

    print('  Wind speed: {} m/s ({:.2f} km/h)'.format(wind_speed_m_s, wind_speed_km_h))


if __name__ == '__main__':
    args = process_args()

    if args.verbose:
        # pass the filename (without the extension) into the logging initialisation
        setup_logging(os.path.splitext(os.path.basename(__file__))[0])

    # log which arguments were passed
    logging.debug(args)
    main(args)
